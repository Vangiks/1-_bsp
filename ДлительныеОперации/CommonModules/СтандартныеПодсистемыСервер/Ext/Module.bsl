﻿// Возвращает фиксированное соответствие, содержащее некоторые параметры клиента:
//  ПараметрЗапуска                    - Строка,
//  СтрокаСоединенияИнформационнойБазы - Строка - строка соединения, полученная на клиенте.
//
// Возвращает пустое фиксированное соответствие, если ТекущийРежимЗапуска() = Неопределено.
//
Функция ПараметрыКлиентаНаСервере() Экспорт
	
	УстановитьПривилегированныйРежим(Истина);
	ПараметрыКлиента = ПараметрыСеанса.ПараметрыКлиентаНаСервере;
	УстановитьПривилегированныйРежим(Ложь);
	
	Если ПараметрыКлиента.Количество() = 0
	   И ТекущийРежимЗапуска() <> Неопределено Тогда
		
		ВызватьИсключение НСтр("ru = 'Не заполнены параметры клиента на сервере.'");
	КонецЕсли;
	
	Возврат ПараметрыКлиента;
	
КонецФункции

// Для вызова из обработчика УстановкаПараметровСеанса модуля сеанса.
//
// Параметры:
//  ИменаПараметровСеанса - Массив, Неопределено - в массиве имена параметров сеанса для инициализации.
//
//  Возвращает массив имен установленных параметров сеанса.
//
Функция УстановкаПараметровСеанса(ИменаПараметровСеанса) Экспорт
	
	// Параметры сеанса, инициализация которых требует обращения к одним и тем же данным
	// следует инициализировать сразу группой. Для того, чтобы избежать их повторной инициализации,
	// имена уже установленных параметров сеанса сохраняются в массиве УстановленныеПараметры.
	УстановленныеПараметры = Новый Массив;
	
	Если ИменаПараметровСеанса = Неопределено Тогда
		ПараметрыСеанса.ПараметрыКлиентаНаСервере = Новый ФиксированноеСоответствие(Новый Соответствие);
		// убрал
		//Справочники.ВерсииРасширений.УстановкаПараметровСеанса(ИменаПараметровСеанса, УстановленныеПараметры);
		
		// При установке соединения с информационной базой до вызова всех остальных обработчиков.
		// убрал
		//ПередЗапускомПрограммы();
		Возврат УстановленныеПараметры;
	КонецЕсли;
	
	Если ИменаПараметровСеанса.Найти("ПараметрыКлиентаНаСервере") <> Неопределено Тогда
		ПараметрыСеанса.ПараметрыКлиентаНаСервере = Новый ФиксированноеСоответствие(Новый Соответствие);
		УстановленныеПараметры.Добавить("ПараметрыКлиентаНаСервере");
	КонецЕсли;
	
	Возврат УстановленныеПараметры;
	
КонецФункции

//Процедура ПередЗапускомПрограммы()
//	
//	// Привилегированный режим (установлен платформой).
//	
//	// Проверка основного языка программирования, установленного в конфигурации.
//	Если Метаданные.ВариантВстроенногоЯзыка <> Метаданные.СвойстваОбъектов.ВариантВстроенногоЯзыка.Русский Тогда
//		ВызватьИсключение СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
//			НСтр("ru = 'Вариант встроенного языка конфигурации ""%1"" не поддерживается.
//			           |Необходимо использовать вариант языка ""%2"".'"),
//			Метаданные.ВариантВстроенногоЯзыка,
//			Метаданные.СвойстваОбъектов.ВариантВстроенногоЯзыка.Русский);
//	КонецЕсли;
//		
//	// Проверка настройки совместимости конфигурации с версией платформы.
//	СистемнаяИнформация = Новый СистемнаяИнформация;
//	МинимальнаяВерсияПлатформы = "8.3.8.1652";
//	Если ОбщегоНазначенияКлиентСервер.СравнитьВерсии(СистемнаяИнформация.ВерсияПриложения, МинимальнаяВерсияПлатформы) < 0 Тогда
//		ВызватьИсключение СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
//			НСтр("ru = 'Для запуска необходима версия платформы 1С:Предприятие %1 или выше.'"), МинимальнаяВерсияПлатформы);
//	КонецЕсли;
//	
//	Режимы = Метаданные.СвойстваОбъектов.РежимСовместимости;
//	ТекущийРежим = Метаданные.РежимСовместимости;
//	
//	Если ТекущийРежим = Режимы.НеИспользовать Тогда
//		НедоступныйРежим = "";
//	ИначеЕсли ТекущийРежим = Режимы.Версия8_1 Тогда
//		НедоступныйРежим = "8.1"
//	ИначеЕсли ТекущийРежим = Режимы.Версия8_2_13 Тогда
//		НедоступныйРежим = "8.2.13"
//	ИначеЕсли ТекущийРежим = Режимы.Версия8_2_16 Тогда
//		НедоступныйРежим = "8.2.16";
//	ИначеЕсли ТекущийРежим = Режимы.Версия8_3_1 Тогда
//		НедоступныйРежим = "8.3.1";
//	ИначеЕсли ТекущийРежим = Режимы.Версия8_3_2 Тогда
//		НедоступныйРежим = "8.3.2";
//	ИначеЕсли ТекущийРежим = Режимы.Версия8_3_3 Тогда
//		НедоступныйРежим = "8.3.3";
//	ИначеЕсли ТекущийРежим = Режимы.Версия8_3_4 Тогда
//		НедоступныйРежим = "8.3.4";
//	ИначеЕсли ТекущийРежим = Режимы.Версия8_3_5 Тогда
//		НедоступныйРежим = "8.3.5";
//	ИначеЕсли ТекущийРежим = Режимы.Версия8_3_6 Тогда
//		НедоступныйРежим = "8.3.6";
//	ИначеЕсли ТекущийРежим = Режимы.Версия8_3_7 Тогда
//		НедоступныйРежим = "8.3.7";
//	Иначе
//		НедоступныйРежим = "";
//	КонецЕсли;
//	
//	Если ЗначениеЗаполнено(НедоступныйРежим) Тогда
//		ВызватьИсключение СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
//			НСтр("ru = 'Режим совместимости конфигурации с 1С:Предприятием версии %1 не поддерживается.
//			           |Для запуска установите в конфигурации режим совместимости с 1С:Предприятием
//			           |версии не ниже 8.3.8 или ""Не использовать"".'"),
//			НедоступныйРежим);
//	КонецЕсли;
//	
//	// Проверка заполнения версии конфигурации.
//	Если ПустаяСтрока(Метаданные.Версия) Тогда
//		ВызватьИсключение НСтр("ru = 'Не заполнено свойство конфигурации Версия.'");
//	Иначе
//		Попытка
//			НулеваяВерсия = ОбщегоНазначенияКлиентСервер.СравнитьВерсии(Метаданные.Версия, "0.0.0.0") = 0;
//		Исключение
//			ВызватьИсключение СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
//				НСтр("ru = 'Не правильно заполнено свойство конфигурации Версия: ""%1"".
//				           |Правильный формат, например: ""1.2.3.45"".'"),
//				Метаданные.Версия);
//		КонецПопытки;
//		Если НулеваяВерсия Тогда
//			ВызватьИсключение СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
//				НСтр("ru = 'Не правильно заполнено свойство конфигурации Версия: ""%1"".
//				           |Версия не может быть нулевой.'"),
//				Метаданные.Версия);
//		КонецЕсли;
//	КонецЕсли;
//	
//	Если (Метаданные.ОсновныеРоли.Количество() <> 2 И Метаданные.ОсновныеРоли.Количество() <> 3)
//	 Или Не Метаданные.ОсновныеРоли.Содержит(Метаданные.Роли.АдминистраторСистемы)
//	 Или Не Метаданные.ОсновныеРоли.Содержит(Метаданные.Роли.ПолныеПрава) Тогда
//		ВызватьИсключение
//			НСтр("ru = 'В конфигурации в свойстве ОсновныеРоли не указаны стандартные роли
//			           |АдминистраторСистемы и ПолныеПрава или указаны лишние роли.'");
//	КонецЕсли;
//	
//	// Проверка возможности выполнения обработчиков установки параметров сеанса для запуска программы.
//	РаботаВБезопасномРежиме.ПроверитьВозможностьВыполненияОбработчиковУстановкиПараметровСеанса();
//	
//	Если Не ЗначениеЗаполнено(ПользователиИнформационнойБазы.ТекущийПользователь().Имя)
//	   И (Не ОбщегоНазначенияПовтИсп.РазделениеВключено()
//	      Или Не ОбщегоНазначенияПовтИсп.ДоступноИспользованиеРазделенныхДанных())
//	   И ОбновлениеИнформационнойБазыСлужебный.ВерсияИБ("СтандартныеПодсистемы",
//	       ОбщегоНазначенияПовтИсп.РазделениеВключено()) = "0.0.0.0" Тогда
//		
//		ПользователиСлужебный.УстановитьНачальныеНастройки("");
//	КонецЕсли;
//	
//	Если ОбщегоНазначения.ПодсистемаСуществует("СтандартныеПодсистемы.РаботаВМоделиСервиса.БазоваяФункциональностьВМоделиСервиса") Тогда
//		МодульРаботаВМоделиСервиса = ОбщегоНазначения.ОбщийМодуль("РаботаВМоделиСервиса");
//		МодульРаботаВМоделиСервиса.ПриПроверкеВключенияБезопасногоРежимаРазделенияДанных();
//	КонецЕсли;
//	
//	Если ОбщегоНазначения.ПодсистемаСуществует("СтандартныеПодсистемы.РаботаВМоделиСервиса.РезервноеКопированиеОбластейДанных") Тогда
//		// Установка флага активности пользователей в области.
//		МодульРезервноеКопированиеОбластейДанных = ОбщегоНазначения.ОбщийМодуль("РезервноеКопированиеОбластейДанных");
//		МодульРезервноеКопированиеОбластейДанных.УстановитьФлагАктивностиПользователяВОбласти();
//	КонецЕсли;
//	
//	ОчиститьНачальнуюСтраницуНеразделенногоПользователяПриПервомВходе();
//	
//КонецПроцедуры